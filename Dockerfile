FROM gitlab/dind:latest

LABEL maintainer "Trusting Social <sre@trustingsocial.com"

ENV DOCKER_VER=17.03.1-ce
ENV DOCKER_COMPOSE_VER=1.22.0
# ENV DOCKER_HOST tcp://docker:2375

# Base Installation
RUN apt-get -qq update && \
    apt-get install -y build-essential \
    python python-pip \
    # libssl-dev libffi-dev libxml2-dev libxslt1-dev zlib1g-dev \
    # python python-pip \
    # python-dev \
    # iptables \
    make \
    git-core \
    wget \
    curl \
    unzip

# Docker
#RUN set -x && \
#    curl -L -o /tmp/docker.tgz https://get.docker.com/builds/Linux/x86_64/docker-${DOCKER_VER}.tgz && \
#    tar -xz -C /tmp -f /tmp/docker.tgz && \
#    mv /tmp/docker/* /usr/local/bin/ && \
#    chmod +x /usr/local/bin/docker && \
#    rm -rf /var/log/* && rm -rf /tmp/*
    
    # mv /tmp/docker/* /usr/bin
# RUN usermod -aG docker ${USER}

# RUN curl -L -o "https://get.docker.com/builds/Linux/x86_64/docker-${DOCKER_VERSION}.tgz" -o /tmp/docker.tgz && \
#     tar -xzvf /tmp/docker.tgz && \
#     mv docker/* /usr/local/bin/ && \
#     chmod +x /usr/local/bin/docker && \
#     # pip install --cert /home/jenkins/www.digicert.com.pem docker-compose && \
#     rm -rf /var/log/* && rm -rf /tmp/*

# Docker-compose
#RUN curl -L https://github.com/docker/compose/releases/download/${DOCKER_COMPOSE_VER}/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose && \
#    chmod +x /usr/local/bin/docker-compose

# Golang
RUN pip install docker-compose
RUN curl -sSL https://storage.googleapis.com/golang/go1.10.linux-amd64.tar.gz | tar -v -C /usr/local -xz
ENV GOPATH /go
ENV GOROOT /usr/local/go
ENV PATH /usr/local/go/bin:/go/bin:/usr/local/bin:$PATH

# Terraform
RUN curl -fSL "https://releases.hashicorp.com/terraform/0.11.7/terraform_0.11.7_linux_amd64.zip" -o terraform.zip
RUN mkdir -p /opt/terraform && unzip terraform.zip -d /opt/terraform
RUN ln -s /opt/terraform/terraform /usr/bin/terraform

# Protobuf
ENV PROTO_VERSION=3.5.1
ENV PROTO_DIR="/tmp/proto/$PROTO_VERSION"

RUN mkdir -p ${PROTO_DIR}
RUN wget "https://github.com/google/protobuf/releases/download/v${PROTO_VERSION}/protoc-${PROTO_VERSION}-linux-x86_64.zip" -O /tmp/proto.zip
RUN unzip /tmp/proto.zip -d ${PROTO_DIR}
RUN rm /tmp/proto.zip


# Test
RUN go env
RUN docker -v
RUN docker-compose version
RUN terraform version
# RUN make
# RUN git

CMD ["bash"]

