FROM alpine:3.8

RUN apk --update add docker make py-pip
RUN pip install docker-compose

# Test Installation
RUN git
RUN go env
RUN go version 
RUN docker-compose -v

CMD ["bash"]
